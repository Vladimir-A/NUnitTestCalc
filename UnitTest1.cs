using NUnit.Framework;
using System.Collections.Generic;

namespace NUnitTestCalc
{
    public class Tests
    {
        public Calculator.Methods calc = new Calculator.Methods();

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestAddition()
        {
            List<string> ls = new List<string>() {"\n"};

            for (double i = 0; i <= 20; i++)
            {
                for (double j = 0; j <= 20; j++)
                {
                    if (calc.addition(i, j) != (i + j))
                        ls.Add(i.ToString() + " + " + j.ToString() + " != " + calc.addition(i, j).ToString() + "\n");
                }
            }

            if (ls.Count != 0)
            {
                Assert.Fail("CountOfErrors - " + ls.Count.ToString() + "\n\n" + "[{0}]", string.Join("\n", ls));
            }

            Assert.Pass("Done");
        }

        [Test]
        public void TestDifference()
        {
            List<string> ls = new List<string>() { "\n" };

            for (double i = 0; i <= 20; i++)
            {
                for (double j = 0; j <= 20; j++)
                {
                    if (calc.difference(i, j) != (i - j))
                        ls.Add(i.ToString() + " - " + j.ToString() + " != " + calc.difference(i, j).ToString() + "\n");
                }
            }

            if (ls.Count != 0)
            {
                Assert.Fail("CountOfErrors - " + ls.Count.ToString() + "\n\n" + "[{0}]", string.Join("\n", ls));
            }

            Assert.Pass("Done");
        }

        [Test]
        public void TestMultiplication()
        {
            List<string> ls = new List<string>() { "\n" };

            for (double i = 0; i <= 20; i++)
            {
                for (double j = 0; j <= 20; j++)
                {
                    if (calc.multiplication(i, j) != (i * j))
                        ls.Add(i.ToString() + " * " + j.ToString() + " != " + calc.multiplication(i, j).ToString() + "\n");
                }
            }

            if (ls.Count != 0)
            {
                Assert.Fail("CountOfErrors - " + ls.Count.ToString() + "\n\n" + "[{0}]", string.Join("\n", ls));
            }

            Assert.Pass("Done");
        }

        [Test]
        public void TestDivision()
        {
            List<string> ls = new List<string>() { "\n" };

            for (double i = 0; i <= 20; i++)
            {
                for (double j = 1; j <= 20; j++)
                {
                    if (calc.division(i, j) != (i / j))
                        ls.Add(i.ToString() + " / " + j.ToString() + " != " + calc.division(i, j).ToString() + "\n");
                }
            }

            if (ls.Count != 0)
            {
                Assert.Fail("CountOfErrors - " + ls.Count.ToString() + "\n\n" + "[{0}]", string.Join("\n", ls));
            }

            Assert.Pass("Done");
        }

        [Test]
        public void TestAddition_2()
        {
            List<string> ls = new List<string>() { "\n" };

            for (double i = 0; i <= 20; i++)
            {
                for (double j = 1; j <= 20; j++)
                {
                    if (Calculator.Calculator.calculator(i,j,"+") != (i + j))
                        ls.Add(i.ToString() + " + " + j.ToString() + " != " + Calculator.Calculator.calculator(i, j, "+").ToString() + "\n");
                }
            }

            if (ls.Count != 0)
            {
                Assert.Fail("CountOfErrors - " + ls.Count.ToString() + "\n\n" + "[{0}]", string.Join("\n", ls));
            }

            Assert.Pass("Done");
        }

        [Test]
        public void TestDifference_2()
        {
            List<string> ls = new List<string>() { "\n" };

            for (double i = 0; i <= 20; i++)
            {
                for (double j = 0; j <= 20; j++)
                {
                    if (Calculator.Calculator.calculator(i, j, "-") != (i - j))
                        ls.Add(i.ToString() + " - " + j.ToString() + " != " + Calculator.Calculator.calculator(i, j, "-").ToString() + "\n");
                }
            }

            if (ls.Count != 0)
            {
                Assert.Fail("CountOfErrors - " + ls.Count.ToString() + "\n\n" + "[{0}]", string.Join("\n", ls));
            }

            Assert.Pass("Done");
        }

        [Test]
        public void TestMultiplication_2()
        {
            List<string> ls = new List<string>() { "\n" };

            for (double i = 0; i <= 20; i++)
            {
                for (double j = 0; j <= 20; j++)
                {
                    if (Calculator.Calculator.calculator(i, j, "*") != (i * j))
                        ls.Add(i.ToString() + " * " + j.ToString() + " != " + Calculator.Calculator.calculator(i, j, "*").ToString() + "\n");
                }
            }

            if (ls.Count != 0)
            {
                Assert.Fail("CountOfErrors - " + ls.Count.ToString() + "\n\n" + "[{0}]", string.Join("\n", ls));
            }

            Assert.Pass("Done");
        }

        [Test]
        public void TestDivision_2()
        {
            List<string> ls = new List<string>() { "\n" };

            for (double i = 0; i <= 20; i++)
            {
                for (double j = 1; j <= 20; j++)
                {
                    if (Calculator.Calculator.calculator(i, j, "/") != (i / j))
                        ls.Add(i.ToString() + " / " + j.ToString() + " != " + Calculator.Calculator.calculator(i, j, "/").ToString() + "\n");
                }
            }

            if (ls.Count != 0)
            {
                Assert.Fail("CountOfErrors - " + ls.Count.ToString() + "\n\n" + "[{0}]", string.Join("\n", ls));
            }

            Assert.Pass("Done");
        }
    }
}